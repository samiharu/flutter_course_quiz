import 'package:flutter/material.dart';

class Result extends StatelessWidget {
  final String resultScore;
  final Function resetHandler;

  Result(this.resultScore, this.resetHandler);

  static const _resultArray = {
    'aaaa': 'Максим Горький (Логико-сенсорный интроверт, ЛСИ)',
    'aaab': 'Штирлиц (Логико-сенсорный экстраверт, ЛСЭ)',
    'aabb': 'Джек Лондон (Логико-интуитивный экстраверт, ЛИЭ)',
    'abbb': 'Гамлет (Этико-интуитивный экстраверт, ЭИЭ)',
    'bbbb': 'Гексли (Интуитивно-этический экстраверт, ИЭЭ)',
    'bbba': 'Есенин (Интуитивно-этический, ИЭИ)',
    'bbaa': 'Дюма (Сенсорно-этический интроверт, СЭИ)',
    'baaa': 'Габен (Сенсорно-логический интроверт, СЛИ)',
    'abab': 'Гюго (Этико-сенсорный экстраверт, ЭСЭ)',
    'baba': 'Бальзак (Интуитивно-логический интроверт, ИЛИ)',
    'abba': 'Достоевский (Этико-интуитивный интроверт, ЭИИ)',
    'baab': 'Жуков (Сенсорно-логический экстраверт, СЛЭ)',
    'bbab': 'Наполеон (Сенсорно-этический экстраверт, СЭЭ)',
    'babb': 'Дон Кихот (Интуитивно-логический экстраверт, ИЛЭ)',
    'aaba': 'Робеспьер (Логико-интуитивный интроверт, ЛИИ)',
    'abaa': 'Драйзер (Этико-сенсорный интроверт, ЭСИ)',
  };

  String get resultPhrase {
    var resultText = _resultArray[resultScore];
    return resultText;
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        children: [
          Text(
            resultPhrase,
            style: TextStyle(fontSize: 36, fontWeight: FontWeight.bold),
            textAlign: TextAlign.center,
          ),
          FlatButton(onPressed: resetHandler, child: Text('Restart Quiz')),
        ],
      ),
    );
  }
}
