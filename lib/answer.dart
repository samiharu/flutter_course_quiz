import 'package:flutter/material.dart';

class Answer extends StatelessWidget {
  final VoidCallback selectHandler;
  final String answerText;

  Answer(this.selectHandler, this.answerText);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      child: Container(
        margin: EdgeInsets.fromLTRB(10, 0, 10, 10),
        child: ElevatedButton(
          style: ElevatedButton.styleFrom(
            primary: Color.fromRGBO(230, 242, 255, 2),
            onPrimary: Colors.black,
            padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
            textStyle: const TextStyle(fontSize: 15, fontFamily: 'RobotoMono'),
          ),
          child: Text(answerText),
          onPressed: selectHandler,
        ),
      ),
    );
  }
}
